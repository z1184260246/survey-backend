package com.aim.questionnaire.service;

import com.aim.questionnaire.common.utils.UUIDUtil;
import com.aim.questionnaire.dao.SurveyAnswerMapper;
import com.aim.questionnaire.dao.entity.SurveyAnswer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@Service
public class SurveyAnswerService {

    @Autowired
    private SurveyAnswerMapper surveyAnswerMapper;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public int answerSurvey(Map<String, Object> map) {
        LocalDateTime startTime = LocalDateTime.parse((String) map.get("answerStartTime"), formatter);
        LocalDateTime endTime = LocalDateTime.parse((String) map.get("answerEndTime"), formatter);
        SurveyAnswer surveyAnswer = SurveyAnswer.bBuild()
                .id(UUIDUtil.getOneUUID())
                .answer((String) map.get("answer"))
                .surveyId((String) map.get("surveyId"))
                .ip((String) map.get("ipAddress"))
                .startTime(startTime)
                .endTime(endTime)
                .build();
        surveyAnswerMapper.insertSurveyAnswer(surveyAnswer);
        return 0;
    }

    public List<SurveyAnswer> querySurveyResult(String surveyId) {
        SurveyAnswer query = SurveyAnswer.queryBuild().surveyId(surveyId).fetchAll().build();
        return surveyAnswerMapper.querySurveyAnswer(query);
    }
}
