package com.aim.questionnaire.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.SurveyLinked;
import com.aim.questionnaire.dao.base.SurveyLinkedBaseMapper;
/**
*  @author author
*/
public interface SurveyLinkedMapper extends SurveyLinkedBaseMapper{

    int deleteSurveyLinked(@Param("id") String id);

}
