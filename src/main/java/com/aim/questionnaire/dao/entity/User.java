package com.aim.questionnaire.dao.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class User implements Serializable {

    private static final long serialVersionUID = 1667311892433L;


    /**
    * 主键
    * 
    * isNullAble:0
    */
    private String id;

    /**
    * 
    * isNullAble:1
    */
    private String tenant_id;


    public void setId(String id){this.id = id;}

    public String getId(){return this.id;}

    public void setTenant_id(String tenant_id){this.tenant_id = tenant_id;}

    public String getTenant_id(){return this.tenant_id;}
    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                "tenant_id='" + tenant_id + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private User set;

        private ConditionBuilder where;

        public UpdateBuilder set(User set){
            this.set = set;
            return this;
        }

        public User getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends User{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> tenant_idList;

        public List<String> getTenant_idList(){return this.tenant_idList;}


        private List<String> fuzzyTenant_id;

        public List<String> getFuzzyTenant_id(){return this.fuzzyTenant_id;}

        private List<String> rightFuzzyTenant_id;

        public List<String> getRightFuzzyTenant_id(){return this.rightFuzzyTenant_id;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public QueryBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public QueryBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public QueryBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public QueryBuilder id(String id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder fuzzyTenant_id (List<String> fuzzyTenant_id){
            this.fuzzyTenant_id = fuzzyTenant_id;
            return this;
        }

        public QueryBuilder fuzzyTenant_id (String ... fuzzyTenant_id){
            this.fuzzyTenant_id = solveNullList(fuzzyTenant_id);
            return this;
        }

        public QueryBuilder rightFuzzyTenant_id (List<String> rightFuzzyTenant_id){
            this.rightFuzzyTenant_id = rightFuzzyTenant_id;
            return this;
        }

        public QueryBuilder rightFuzzyTenant_id (String ... rightFuzzyTenant_id){
            this.rightFuzzyTenant_id = solveNullList(rightFuzzyTenant_id);
            return this;
        }

        public QueryBuilder tenant_id(String tenant_id){
            setTenant_id(tenant_id);
            return this;
        }

        public QueryBuilder tenant_idList(String ... tenant_id){
            this.tenant_idList = solveNullList(tenant_id);
            return this;
        }

        public QueryBuilder tenant_idList(List<String> tenant_id){
            this.tenant_idList = tenant_id;
            return this;
        }

        public QueryBuilder fetchTenant_id(){
            setFetchFields("fetchFields","tenant_id");
            return this;
        }

        public QueryBuilder excludeTenant_id(){
            setFetchFields("excludeFields","tenant_id");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public User build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> tenant_idList;

        public List<String> getTenant_idList(){return this.tenant_idList;}


        private List<String> fuzzyTenant_id;

        public List<String> getFuzzyTenant_id(){return this.fuzzyTenant_id;}

        private List<String> rightFuzzyTenant_id;

        public List<String> getRightFuzzyTenant_id(){return this.rightFuzzyTenant_id;}

        public ConditionBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public ConditionBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public ConditionBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public ConditionBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public ConditionBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder fuzzyTenant_id (List<String> fuzzyTenant_id){
            this.fuzzyTenant_id = fuzzyTenant_id;
            return this;
        }

        public ConditionBuilder fuzzyTenant_id (String ... fuzzyTenant_id){
            this.fuzzyTenant_id = solveNullList(fuzzyTenant_id);
            return this;
        }

        public ConditionBuilder rightFuzzyTenant_id (List<String> rightFuzzyTenant_id){
            this.rightFuzzyTenant_id = rightFuzzyTenant_id;
            return this;
        }

        public ConditionBuilder rightFuzzyTenant_id (String ... rightFuzzyTenant_id){
            this.rightFuzzyTenant_id = solveNullList(rightFuzzyTenant_id);
            return this;
        }

        public ConditionBuilder tenant_idList(String ... tenant_id){
            this.tenant_idList = solveNullList(tenant_id);
            return this;
        }

        public ConditionBuilder tenant_idList(List<String> tenant_id){
            this.tenant_idList = tenant_id;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private User obj;

        public Builder(){
            this.obj = new User();
        }

        public Builder id(String id){
            this.obj.setId(id);
            return this;
        }
        public Builder tenant_id(String tenant_id){
            this.obj.setTenant_id(tenant_id);
            return this;
        }
        public User build(){return obj;}
    }

}
