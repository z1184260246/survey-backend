package com.aim.questionnaire.dao.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class Answer implements Serializable {

    private static final long serialVersionUID = 1667311875691L;


    /**
    * 主键
    * 
    * isNullAble:0
    */
    private String id;

    /**
    * 
    * isNullAble:1
    */
    private String group_id;


    public void setId(String id){this.id = id;}

    public String getId(){return this.id;}

    public void setGroup_id(String group_id){this.group_id = group_id;}

    public String getGroup_id(){return this.group_id;}
    @Override
    public String toString() {
        return "Answer{" +
                "id='" + id + '\'' +
                "group_id='" + group_id + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private Answer set;

        private ConditionBuilder where;

        public UpdateBuilder set(Answer set){
            this.set = set;
            return this;
        }

        public Answer getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends Answer{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> group_idList;

        public List<String> getGroup_idList(){return this.group_idList;}


        private List<String> fuzzyGroup_id;

        public List<String> getFuzzyGroup_id(){return this.fuzzyGroup_id;}

        private List<String> rightFuzzyGroup_id;

        public List<String> getRightFuzzyGroup_id(){return this.rightFuzzyGroup_id;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public QueryBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public QueryBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public QueryBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public QueryBuilder id(String id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder fuzzyGroup_id (List<String> fuzzyGroup_id){
            this.fuzzyGroup_id = fuzzyGroup_id;
            return this;
        }

        public QueryBuilder fuzzyGroup_id (String ... fuzzyGroup_id){
            this.fuzzyGroup_id = solveNullList(fuzzyGroup_id);
            return this;
        }

        public QueryBuilder rightFuzzyGroup_id (List<String> rightFuzzyGroup_id){
            this.rightFuzzyGroup_id = rightFuzzyGroup_id;
            return this;
        }

        public QueryBuilder rightFuzzyGroup_id (String ... rightFuzzyGroup_id){
            this.rightFuzzyGroup_id = solveNullList(rightFuzzyGroup_id);
            return this;
        }

        public QueryBuilder group_id(String group_id){
            setGroup_id(group_id);
            return this;
        }

        public QueryBuilder group_idList(String ... group_id){
            this.group_idList = solveNullList(group_id);
            return this;
        }

        public QueryBuilder group_idList(List<String> group_id){
            this.group_idList = group_id;
            return this;
        }

        public QueryBuilder fetchGroup_id(){
            setFetchFields("fetchFields","group_id");
            return this;
        }

        public QueryBuilder excludeGroup_id(){
            setFetchFields("excludeFields","group_id");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public Answer build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> group_idList;

        public List<String> getGroup_idList(){return this.group_idList;}


        private List<String> fuzzyGroup_id;

        public List<String> getFuzzyGroup_id(){return this.fuzzyGroup_id;}

        private List<String> rightFuzzyGroup_id;

        public List<String> getRightFuzzyGroup_id(){return this.rightFuzzyGroup_id;}

        public ConditionBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public ConditionBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public ConditionBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public ConditionBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public ConditionBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder fuzzyGroup_id (List<String> fuzzyGroup_id){
            this.fuzzyGroup_id = fuzzyGroup_id;
            return this;
        }

        public ConditionBuilder fuzzyGroup_id (String ... fuzzyGroup_id){
            this.fuzzyGroup_id = solveNullList(fuzzyGroup_id);
            return this;
        }

        public ConditionBuilder rightFuzzyGroup_id (List<String> rightFuzzyGroup_id){
            this.rightFuzzyGroup_id = rightFuzzyGroup_id;
            return this;
        }

        public ConditionBuilder rightFuzzyGroup_id (String ... rightFuzzyGroup_id){
            this.rightFuzzyGroup_id = solveNullList(rightFuzzyGroup_id);
            return this;
        }

        public ConditionBuilder group_idList(String ... group_id){
            this.group_idList = solveNullList(group_id);
            return this;
        }

        public ConditionBuilder group_idList(List<String> group_id){
            this.group_idList = group_id;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private Answer obj;

        public Builder(){
            this.obj = new Answer();
        }

        public Builder id(String id){
            this.obj.setId(id);
            return this;
        }
        public Builder group_id(String group_id){
            this.obj.setGroup_id(group_id);
            return this;
        }
        public Answer build(){return obj;}
    }

}
