package com.aim.questionnaire.dao.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class SurveyTemplate implements Serializable {

    private static final long serialVersionUID = 1656464053344L;


    /**
    * 主键
    * 问卷模板表主键
    * isNullAble:0
    */
    private String id;

    /**
    * 问卷名称
    * isNullAble:1
    */
    private String surveyName;

    /**
    * 问卷内容说明
    * isNullAble:1
    */
    private String surveyContent;


    public void setId(String id){this.id = id;}

    public String getId(){return this.id;}

    public void setSurveyName(String surveyName){this.surveyName = surveyName;}

    public String getSurveyName(){return this.surveyName;}

    public void setSurveyContent(String surveyContent){this.surveyContent = surveyContent;}

    public String getSurveyContent(){return this.surveyContent;}
    @Override
    public String toString() {
        return "SurveyTemplate{" +
                "id='" + id + '\'' +
                "surveyName='" + surveyName + '\'' +
                "surveyContent='" + surveyContent + '\'' +
            '}';
    }

    public static Builder bBuild(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder queryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private SurveyTemplate set;

        private ConditionBuilder where;

        public UpdateBuilder set(SurveyTemplate set){
            this.set = set;
            return this;
        }

        public SurveyTemplate getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends SurveyTemplate{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> surveyNameList;

        public List<String> getSurveyNameList(){return this.surveyNameList;}


        private List<String> fuzzySurveyName;

        public List<String> getFuzzySurveyName(){return this.fuzzySurveyName;}

        private List<String> rightFuzzySurveyName;

        public List<String> getRightFuzzySurveyName(){return this.rightFuzzySurveyName;}
        private List<String> surveyContentList;

        public List<String> getSurveyContentList(){return this.surveyContentList;}


        private List<String> fuzzySurveyContent;

        public List<String> getFuzzySurveyContent(){return this.fuzzySurveyContent;}

        private List<String> rightFuzzySurveyContent;

        public List<String> getRightFuzzySurveyContent(){return this.rightFuzzySurveyContent;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public QueryBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public QueryBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public QueryBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public QueryBuilder id(String id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder fuzzySurveyName (List<String> fuzzySurveyName){
            this.fuzzySurveyName = fuzzySurveyName;
            return this;
        }

        public QueryBuilder fuzzySurveyName (String ... fuzzySurveyName){
            this.fuzzySurveyName = solveNullList(fuzzySurveyName);
            return this;
        }

        public QueryBuilder rightFuzzySurveyName (List<String> rightFuzzySurveyName){
            this.rightFuzzySurveyName = rightFuzzySurveyName;
            return this;
        }

        public QueryBuilder rightFuzzySurveyName (String ... rightFuzzySurveyName){
            this.rightFuzzySurveyName = solveNullList(rightFuzzySurveyName);
            return this;
        }

        public QueryBuilder surveyName(String surveyName){
            setSurveyName(surveyName);
            return this;
        }

        public QueryBuilder surveyNameList(String ... surveyName){
            this.surveyNameList = solveNullList(surveyName);
            return this;
        }

        public QueryBuilder surveyNameList(List<String> surveyName){
            this.surveyNameList = surveyName;
            return this;
        }

        public QueryBuilder fetchSurveyName(){
            setFetchFields("fetchFields","surveyName");
            return this;
        }

        public QueryBuilder excludeSurveyName(){
            setFetchFields("excludeFields","surveyName");
            return this;
        }

        public QueryBuilder fuzzySurveyContent (List<String> fuzzySurveyContent){
            this.fuzzySurveyContent = fuzzySurveyContent;
            return this;
        }

        public QueryBuilder fuzzySurveyContent (String ... fuzzySurveyContent){
            this.fuzzySurveyContent = solveNullList(fuzzySurveyContent);
            return this;
        }

        public QueryBuilder rightFuzzySurveyContent (List<String> rightFuzzySurveyContent){
            this.rightFuzzySurveyContent = rightFuzzySurveyContent;
            return this;
        }

        public QueryBuilder rightFuzzySurveyContent (String ... rightFuzzySurveyContent){
            this.rightFuzzySurveyContent = solveNullList(rightFuzzySurveyContent);
            return this;
        }

        public QueryBuilder surveyContent(String surveyContent){
            setSurveyContent(surveyContent);
            return this;
        }

        public QueryBuilder surveyContentList(String ... surveyContent){
            this.surveyContentList = solveNullList(surveyContent);
            return this;
        }

        public QueryBuilder surveyContentList(List<String> surveyContent){
            this.surveyContentList = surveyContent;
            return this;
        }

        public QueryBuilder fetchSurveyContent(){
            setFetchFields("fetchFields","surveyContent");
            return this;
        }

        public QueryBuilder excludeSurveyContent(){
            setFetchFields("excludeFields","surveyContent");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public SurveyTemplate build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> surveyNameList;

        public List<String> getSurveyNameList(){return this.surveyNameList;}


        private List<String> fuzzySurveyName;

        public List<String> getFuzzySurveyName(){return this.fuzzySurveyName;}

        private List<String> rightFuzzySurveyName;

        public List<String> getRightFuzzySurveyName(){return this.rightFuzzySurveyName;}
        private List<String> surveyContentList;

        public List<String> getSurveyContentList(){return this.surveyContentList;}


        private List<String> fuzzySurveyContent;

        public List<String> getFuzzySurveyContent(){return this.fuzzySurveyContent;}

        private List<String> rightFuzzySurveyContent;

        public List<String> getRightFuzzySurveyContent(){return this.rightFuzzySurveyContent;}

        public ConditionBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public ConditionBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public ConditionBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public ConditionBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public ConditionBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder fuzzySurveyName (List<String> fuzzySurveyName){
            this.fuzzySurveyName = fuzzySurveyName;
            return this;
        }

        public ConditionBuilder fuzzySurveyName (String ... fuzzySurveyName){
            this.fuzzySurveyName = solveNullList(fuzzySurveyName);
            return this;
        }

        public ConditionBuilder rightFuzzySurveyName (List<String> rightFuzzySurveyName){
            this.rightFuzzySurveyName = rightFuzzySurveyName;
            return this;
        }

        public ConditionBuilder rightFuzzySurveyName (String ... rightFuzzySurveyName){
            this.rightFuzzySurveyName = solveNullList(rightFuzzySurveyName);
            return this;
        }

        public ConditionBuilder surveyNameList(String ... surveyName){
            this.surveyNameList = solveNullList(surveyName);
            return this;
        }

        public ConditionBuilder surveyNameList(List<String> surveyName){
            this.surveyNameList = surveyName;
            return this;
        }

        public ConditionBuilder fuzzySurveyContent (List<String> fuzzySurveyContent){
            this.fuzzySurveyContent = fuzzySurveyContent;
            return this;
        }

        public ConditionBuilder fuzzySurveyContent (String ... fuzzySurveyContent){
            this.fuzzySurveyContent = solveNullList(fuzzySurveyContent);
            return this;
        }

        public ConditionBuilder rightFuzzySurveyContent (List<String> rightFuzzySurveyContent){
            this.rightFuzzySurveyContent = rightFuzzySurveyContent;
            return this;
        }

        public ConditionBuilder rightFuzzySurveyContent (String ... rightFuzzySurveyContent){
            this.rightFuzzySurveyContent = solveNullList(rightFuzzySurveyContent);
            return this;
        }

        public ConditionBuilder surveyContentList(String ... surveyContent){
            this.surveyContentList = solveNullList(surveyContent);
            return this;
        }

        public ConditionBuilder surveyContentList(List<String> surveyContent){
            this.surveyContentList = surveyContent;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private SurveyTemplate obj;

        public Builder(){
            this.obj = new SurveyTemplate();
        }

        public Builder id(String id){
            this.obj.setId(id);
            return this;
        }
        public Builder surveyName(String surveyName){
            this.obj.setSurveyName(surveyName);
            return this;
        }
        public Builder surveyContent(String surveyContent){
            this.obj.setSurveyContent(surveyContent);
            return this;
        }
        public SurveyTemplate build(){return obj;}
    }

}
