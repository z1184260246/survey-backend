package com.aim.questionnaire.dao.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class TotalRole implements Serializable {

    private static final long serialVersionUID = 1667378548630L;


    /**
    * 
    * isNullAble:1
    */
    private String account;

    /**
    * 主键
    * 
    * isNullAble:0
    */
    private String id;

    /**
    * 
    * isNullAble:1
    */
    private Integer status;

    /**
    * 
    * isNullAble:1
    */
    private String security_question;

    /**
    * 
    * isNullAble:1
    */
    private Integer role;

    /**
    * 
    * isNullAble:1
    */
    private String phone_number;

    /**
    * 
    * isNullAble:1
    */
    private String security_answer;

    /**
    * 
    * isNullAble:1
    */
    private String password;

    /**
    * 
    * isNullAble:1
    */
    private Integer deleted;


    public void setAccount(String account){this.account = account;}

    public String getAccount(){return this.account;}

    public void setId(String id){this.id = id;}

    public String getId(){return this.id;}

    public void setStatus(Integer status){this.status = status;}

    public Integer getStatus(){return this.status;}

    public void setSecurity_question(String security_question){this.security_question = security_question;}

    public String getSecurity_question(){return this.security_question;}

    public void setRole(Integer role){this.role = role;}

    public Integer getRole(){return this.role;}

    public void setPhone_number(String phone_number){this.phone_number = phone_number;}

    public String getPhone_number(){return this.phone_number;}

    public void setSecurity_answer(String security_answer){this.security_answer = security_answer;}

    public String getSecurity_answer(){return this.security_answer;}

    public void setPassword(String password){this.password = password;}

    public String getPassword(){return this.password;}

    public void setDeleted(Integer deleted){this.deleted = deleted;}

    public Integer getDeleted(){return this.deleted;}
    @Override
    public String toString() {
        return "TotalRole{" +
                "account='" + account + '\'' +
                "id='" + id + '\'' +
                "status='" + status + '\'' +
                "security_question='" + security_question + '\'' +
                "role='" + role + '\'' +
                "phone_number='" + phone_number + '\'' +
                "security_answer='" + security_answer + '\'' +
                "password='" + password + '\'' +
                "deleted='" + deleted + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private TotalRole set;

        private ConditionBuilder where;

        public UpdateBuilder set(TotalRole set){
            this.set = set;
            return this;
        }

        public TotalRole getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends TotalRole{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> accountList;

        public List<String> getAccountList(){return this.accountList;}


        private List<String> fuzzyAccount;

        public List<String> getFuzzyAccount(){return this.fuzzyAccount;}

        private List<String> rightFuzzyAccount;

        public List<String> getRightFuzzyAccount(){return this.rightFuzzyAccount;}
        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<Integer> statusList;

        public List<Integer> getStatusList(){return this.statusList;}

        private Integer statusSt;

        private Integer statusEd;

        public Integer getStatusSt(){return this.statusSt;}

        public Integer getStatusEd(){return this.statusEd;}

        private List<String> security_questionList;

        public List<String> getSecurity_questionList(){return this.security_questionList;}


        private List<String> fuzzySecurity_question;

        public List<String> getFuzzySecurity_question(){return this.fuzzySecurity_question;}

        private List<String> rightFuzzySecurity_question;

        public List<String> getRightFuzzySecurity_question(){return this.rightFuzzySecurity_question;}
        private List<Integer> roleList;

        public List<Integer> getRoleList(){return this.roleList;}

        private Integer roleSt;

        private Integer roleEd;

        public Integer getRoleSt(){return this.roleSt;}

        public Integer getRoleEd(){return this.roleEd;}

        private List<String> phone_numberList;

        public List<String> getPhone_numberList(){return this.phone_numberList;}


        private List<String> fuzzyPhone_number;

        public List<String> getFuzzyPhone_number(){return this.fuzzyPhone_number;}

        private List<String> rightFuzzyPhone_number;

        public List<String> getRightFuzzyPhone_number(){return this.rightFuzzyPhone_number;}
        private List<String> security_answerList;

        public List<String> getSecurity_answerList(){return this.security_answerList;}


        private List<String> fuzzySecurity_answer;

        public List<String> getFuzzySecurity_answer(){return this.fuzzySecurity_answer;}

        private List<String> rightFuzzySecurity_answer;

        public List<String> getRightFuzzySecurity_answer(){return this.rightFuzzySecurity_answer;}
        private List<String> passwordList;

        public List<String> getPasswordList(){return this.passwordList;}


        private List<String> fuzzyPassword;

        public List<String> getFuzzyPassword(){return this.fuzzyPassword;}

        private List<String> rightFuzzyPassword;

        public List<String> getRightFuzzyPassword(){return this.rightFuzzyPassword;}
        private List<Integer> deletedList;

        public List<Integer> getDeletedList(){return this.deletedList;}

        private Integer deletedSt;

        private Integer deletedEd;

        public Integer getDeletedSt(){return this.deletedSt;}

        public Integer getDeletedEd(){return this.deletedEd;}

        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyAccount (List<String> fuzzyAccount){
            this.fuzzyAccount = fuzzyAccount;
            return this;
        }

        public QueryBuilder fuzzyAccount (String ... fuzzyAccount){
            this.fuzzyAccount = solveNullList(fuzzyAccount);
            return this;
        }

        public QueryBuilder rightFuzzyAccount (List<String> rightFuzzyAccount){
            this.rightFuzzyAccount = rightFuzzyAccount;
            return this;
        }

        public QueryBuilder rightFuzzyAccount (String ... rightFuzzyAccount){
            this.rightFuzzyAccount = solveNullList(rightFuzzyAccount);
            return this;
        }

        public QueryBuilder account(String account){
            setAccount(account);
            return this;
        }

        public QueryBuilder accountList(String ... account){
            this.accountList = solveNullList(account);
            return this;
        }

        public QueryBuilder accountList(List<String> account){
            this.accountList = account;
            return this;
        }

        public QueryBuilder fetchAccount(){
            setFetchFields("fetchFields","account");
            return this;
        }

        public QueryBuilder excludeAccount(){
            setFetchFields("excludeFields","account");
            return this;
        }

        public QueryBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public QueryBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public QueryBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public QueryBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public QueryBuilder id(String id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder statusBetWeen(Integer statusSt,Integer statusEd){
            this.statusSt = statusSt;
            this.statusEd = statusEd;
            return this;
        }

        public QueryBuilder statusGreaterEqThan(Integer statusSt){
            this.statusSt = statusSt;
            return this;
        }
        public QueryBuilder statusLessEqThan(Integer statusEd){
            this.statusEd = statusEd;
            return this;
        }


        public QueryBuilder status(Integer status){
            setStatus(status);
            return this;
        }

        public QueryBuilder statusList(Integer ... status){
            this.statusList = solveNullList(status);
            return this;
        }

        public QueryBuilder statusList(List<Integer> status){
            this.statusList = status;
            return this;
        }

        public QueryBuilder fetchStatus(){
            setFetchFields("fetchFields","status");
            return this;
        }

        public QueryBuilder excludeStatus(){
            setFetchFields("excludeFields","status");
            return this;
        }

        public QueryBuilder fuzzySecurity_question (List<String> fuzzySecurity_question){
            this.fuzzySecurity_question = fuzzySecurity_question;
            return this;
        }

        public QueryBuilder fuzzySecurity_question (String ... fuzzySecurity_question){
            this.fuzzySecurity_question = solveNullList(fuzzySecurity_question);
            return this;
        }

        public QueryBuilder rightFuzzySecurity_question (List<String> rightFuzzySecurity_question){
            this.rightFuzzySecurity_question = rightFuzzySecurity_question;
            return this;
        }

        public QueryBuilder rightFuzzySecurity_question (String ... rightFuzzySecurity_question){
            this.rightFuzzySecurity_question = solveNullList(rightFuzzySecurity_question);
            return this;
        }

        public QueryBuilder security_question(String security_question){
            setSecurity_question(security_question);
            return this;
        }

        public QueryBuilder security_questionList(String ... security_question){
            this.security_questionList = solveNullList(security_question);
            return this;
        }

        public QueryBuilder security_questionList(List<String> security_question){
            this.security_questionList = security_question;
            return this;
        }

        public QueryBuilder fetchSecurity_question(){
            setFetchFields("fetchFields","security_question");
            return this;
        }

        public QueryBuilder excludeSecurity_question(){
            setFetchFields("excludeFields","security_question");
            return this;
        }

        public QueryBuilder roleBetWeen(Integer roleSt,Integer roleEd){
            this.roleSt = roleSt;
            this.roleEd = roleEd;
            return this;
        }

        public QueryBuilder roleGreaterEqThan(Integer roleSt){
            this.roleSt = roleSt;
            return this;
        }
        public QueryBuilder roleLessEqThan(Integer roleEd){
            this.roleEd = roleEd;
            return this;
        }


        public QueryBuilder role(Integer role){
            setRole(role);
            return this;
        }

        public QueryBuilder roleList(Integer ... role){
            this.roleList = solveNullList(role);
            return this;
        }

        public QueryBuilder roleList(List<Integer> role){
            this.roleList = role;
            return this;
        }

        public QueryBuilder fetchRole(){
            setFetchFields("fetchFields","role");
            return this;
        }

        public QueryBuilder excludeRole(){
            setFetchFields("excludeFields","role");
            return this;
        }

        public QueryBuilder fuzzyPhone_number (List<String> fuzzyPhone_number){
            this.fuzzyPhone_number = fuzzyPhone_number;
            return this;
        }

        public QueryBuilder fuzzyPhone_number (String ... fuzzyPhone_number){
            this.fuzzyPhone_number = solveNullList(fuzzyPhone_number);
            return this;
        }

        public QueryBuilder rightFuzzyPhone_number (List<String> rightFuzzyPhone_number){
            this.rightFuzzyPhone_number = rightFuzzyPhone_number;
            return this;
        }

        public QueryBuilder rightFuzzyPhone_number (String ... rightFuzzyPhone_number){
            this.rightFuzzyPhone_number = solveNullList(rightFuzzyPhone_number);
            return this;
        }

        public QueryBuilder phone_number(String phone_number){
            setPhone_number(phone_number);
            return this;
        }

        public QueryBuilder phone_numberList(String ... phone_number){
            this.phone_numberList = solveNullList(phone_number);
            return this;
        }

        public QueryBuilder phone_numberList(List<String> phone_number){
            this.phone_numberList = phone_number;
            return this;
        }

        public QueryBuilder fetchPhone_number(){
            setFetchFields("fetchFields","phone_number");
            return this;
        }

        public QueryBuilder excludePhone_number(){
            setFetchFields("excludeFields","phone_number");
            return this;
        }

        public QueryBuilder fuzzySecurity_answer (List<String> fuzzySecurity_answer){
            this.fuzzySecurity_answer = fuzzySecurity_answer;
            return this;
        }

        public QueryBuilder fuzzySecurity_answer (String ... fuzzySecurity_answer){
            this.fuzzySecurity_answer = solveNullList(fuzzySecurity_answer);
            return this;
        }

        public QueryBuilder rightFuzzySecurity_answer (List<String> rightFuzzySecurity_answer){
            this.rightFuzzySecurity_answer = rightFuzzySecurity_answer;
            return this;
        }

        public QueryBuilder rightFuzzySecurity_answer (String ... rightFuzzySecurity_answer){
            this.rightFuzzySecurity_answer = solveNullList(rightFuzzySecurity_answer);
            return this;
        }

        public QueryBuilder security_answer(String security_answer){
            setSecurity_answer(security_answer);
            return this;
        }

        public QueryBuilder security_answerList(String ... security_answer){
            this.security_answerList = solveNullList(security_answer);
            return this;
        }

        public QueryBuilder security_answerList(List<String> security_answer){
            this.security_answerList = security_answer;
            return this;
        }

        public QueryBuilder fetchSecurity_answer(){
            setFetchFields("fetchFields","security_answer");
            return this;
        }

        public QueryBuilder excludeSecurity_answer(){
            setFetchFields("excludeFields","security_answer");
            return this;
        }

        public QueryBuilder fuzzyPassword (List<String> fuzzyPassword){
            this.fuzzyPassword = fuzzyPassword;
            return this;
        }

        public QueryBuilder fuzzyPassword (String ... fuzzyPassword){
            this.fuzzyPassword = solveNullList(fuzzyPassword);
            return this;
        }

        public QueryBuilder rightFuzzyPassword (List<String> rightFuzzyPassword){
            this.rightFuzzyPassword = rightFuzzyPassword;
            return this;
        }

        public QueryBuilder rightFuzzyPassword (String ... rightFuzzyPassword){
            this.rightFuzzyPassword = solveNullList(rightFuzzyPassword);
            return this;
        }

        public QueryBuilder password(String password){
            setPassword(password);
            return this;
        }

        public QueryBuilder passwordList(String ... password){
            this.passwordList = solveNullList(password);
            return this;
        }

        public QueryBuilder passwordList(List<String> password){
            this.passwordList = password;
            return this;
        }

        public QueryBuilder fetchPassword(){
            setFetchFields("fetchFields","password");
            return this;
        }

        public QueryBuilder excludePassword(){
            setFetchFields("excludeFields","password");
            return this;
        }

        public QueryBuilder deletedBetWeen(Integer deletedSt,Integer deletedEd){
            this.deletedSt = deletedSt;
            this.deletedEd = deletedEd;
            return this;
        }

        public QueryBuilder deletedGreaterEqThan(Integer deletedSt){
            this.deletedSt = deletedSt;
            return this;
        }
        public QueryBuilder deletedLessEqThan(Integer deletedEd){
            this.deletedEd = deletedEd;
            return this;
        }


        public QueryBuilder deleted(Integer deleted){
            setDeleted(deleted);
            return this;
        }

        public QueryBuilder deletedList(Integer ... deleted){
            this.deletedList = solveNullList(deleted);
            return this;
        }

        public QueryBuilder deletedList(List<Integer> deleted){
            this.deletedList = deleted;
            return this;
        }

        public QueryBuilder fetchDeleted(){
            setFetchFields("fetchFields","deleted");
            return this;
        }

        public QueryBuilder excludeDeleted(){
            setFetchFields("excludeFields","deleted");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public TotalRole build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> accountList;

        public List<String> getAccountList(){return this.accountList;}


        private List<String> fuzzyAccount;

        public List<String> getFuzzyAccount(){return this.fuzzyAccount;}

        private List<String> rightFuzzyAccount;

        public List<String> getRightFuzzyAccount(){return this.rightFuzzyAccount;}
        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<Integer> statusList;

        public List<Integer> getStatusList(){return this.statusList;}

        private Integer statusSt;

        private Integer statusEd;

        public Integer getStatusSt(){return this.statusSt;}

        public Integer getStatusEd(){return this.statusEd;}

        private List<String> security_questionList;

        public List<String> getSecurity_questionList(){return this.security_questionList;}


        private List<String> fuzzySecurity_question;

        public List<String> getFuzzySecurity_question(){return this.fuzzySecurity_question;}

        private List<String> rightFuzzySecurity_question;

        public List<String> getRightFuzzySecurity_question(){return this.rightFuzzySecurity_question;}
        private List<Integer> roleList;

        public List<Integer> getRoleList(){return this.roleList;}

        private Integer roleSt;

        private Integer roleEd;

        public Integer getRoleSt(){return this.roleSt;}

        public Integer getRoleEd(){return this.roleEd;}

        private List<String> phone_numberList;

        public List<String> getPhone_numberList(){return this.phone_numberList;}


        private List<String> fuzzyPhone_number;

        public List<String> getFuzzyPhone_number(){return this.fuzzyPhone_number;}

        private List<String> rightFuzzyPhone_number;

        public List<String> getRightFuzzyPhone_number(){return this.rightFuzzyPhone_number;}
        private List<String> security_answerList;

        public List<String> getSecurity_answerList(){return this.security_answerList;}


        private List<String> fuzzySecurity_answer;

        public List<String> getFuzzySecurity_answer(){return this.fuzzySecurity_answer;}

        private List<String> rightFuzzySecurity_answer;

        public List<String> getRightFuzzySecurity_answer(){return this.rightFuzzySecurity_answer;}
        private List<String> passwordList;

        public List<String> getPasswordList(){return this.passwordList;}


        private List<String> fuzzyPassword;

        public List<String> getFuzzyPassword(){return this.fuzzyPassword;}

        private List<String> rightFuzzyPassword;

        public List<String> getRightFuzzyPassword(){return this.rightFuzzyPassword;}
        private List<Integer> deletedList;

        public List<Integer> getDeletedList(){return this.deletedList;}

        private Integer deletedSt;

        private Integer deletedEd;

        public Integer getDeletedSt(){return this.deletedSt;}

        public Integer getDeletedEd(){return this.deletedEd;}


        public ConditionBuilder fuzzyAccount (List<String> fuzzyAccount){
            this.fuzzyAccount = fuzzyAccount;
            return this;
        }

        public ConditionBuilder fuzzyAccount (String ... fuzzyAccount){
            this.fuzzyAccount = solveNullList(fuzzyAccount);
            return this;
        }

        public ConditionBuilder rightFuzzyAccount (List<String> rightFuzzyAccount){
            this.rightFuzzyAccount = rightFuzzyAccount;
            return this;
        }

        public ConditionBuilder rightFuzzyAccount (String ... rightFuzzyAccount){
            this.rightFuzzyAccount = solveNullList(rightFuzzyAccount);
            return this;
        }

        public ConditionBuilder accountList(String ... account){
            this.accountList = solveNullList(account);
            return this;
        }

        public ConditionBuilder accountList(List<String> account){
            this.accountList = account;
            return this;
        }

        public ConditionBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public ConditionBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public ConditionBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public ConditionBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public ConditionBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder statusBetWeen(Integer statusSt,Integer statusEd){
            this.statusSt = statusSt;
            this.statusEd = statusEd;
            return this;
        }

        public ConditionBuilder statusGreaterEqThan(Integer statusSt){
            this.statusSt = statusSt;
            return this;
        }
        public ConditionBuilder statusLessEqThan(Integer statusEd){
            this.statusEd = statusEd;
            return this;
        }


        public ConditionBuilder statusList(Integer ... status){
            this.statusList = solveNullList(status);
            return this;
        }

        public ConditionBuilder statusList(List<Integer> status){
            this.statusList = status;
            return this;
        }

        public ConditionBuilder fuzzySecurity_question (List<String> fuzzySecurity_question){
            this.fuzzySecurity_question = fuzzySecurity_question;
            return this;
        }

        public ConditionBuilder fuzzySecurity_question (String ... fuzzySecurity_question){
            this.fuzzySecurity_question = solveNullList(fuzzySecurity_question);
            return this;
        }

        public ConditionBuilder rightFuzzySecurity_question (List<String> rightFuzzySecurity_question){
            this.rightFuzzySecurity_question = rightFuzzySecurity_question;
            return this;
        }

        public ConditionBuilder rightFuzzySecurity_question (String ... rightFuzzySecurity_question){
            this.rightFuzzySecurity_question = solveNullList(rightFuzzySecurity_question);
            return this;
        }

        public ConditionBuilder security_questionList(String ... security_question){
            this.security_questionList = solveNullList(security_question);
            return this;
        }

        public ConditionBuilder security_questionList(List<String> security_question){
            this.security_questionList = security_question;
            return this;
        }

        public ConditionBuilder roleBetWeen(Integer roleSt,Integer roleEd){
            this.roleSt = roleSt;
            this.roleEd = roleEd;
            return this;
        }

        public ConditionBuilder roleGreaterEqThan(Integer roleSt){
            this.roleSt = roleSt;
            return this;
        }
        public ConditionBuilder roleLessEqThan(Integer roleEd){
            this.roleEd = roleEd;
            return this;
        }


        public ConditionBuilder roleList(Integer ... role){
            this.roleList = solveNullList(role);
            return this;
        }

        public ConditionBuilder roleList(List<Integer> role){
            this.roleList = role;
            return this;
        }

        public ConditionBuilder fuzzyPhone_number (List<String> fuzzyPhone_number){
            this.fuzzyPhone_number = fuzzyPhone_number;
            return this;
        }

        public ConditionBuilder fuzzyPhone_number (String ... fuzzyPhone_number){
            this.fuzzyPhone_number = solveNullList(fuzzyPhone_number);
            return this;
        }

        public ConditionBuilder rightFuzzyPhone_number (List<String> rightFuzzyPhone_number){
            this.rightFuzzyPhone_number = rightFuzzyPhone_number;
            return this;
        }

        public ConditionBuilder rightFuzzyPhone_number (String ... rightFuzzyPhone_number){
            this.rightFuzzyPhone_number = solveNullList(rightFuzzyPhone_number);
            return this;
        }

        public ConditionBuilder phone_numberList(String ... phone_number){
            this.phone_numberList = solveNullList(phone_number);
            return this;
        }

        public ConditionBuilder phone_numberList(List<String> phone_number){
            this.phone_numberList = phone_number;
            return this;
        }

        public ConditionBuilder fuzzySecurity_answer (List<String> fuzzySecurity_answer){
            this.fuzzySecurity_answer = fuzzySecurity_answer;
            return this;
        }

        public ConditionBuilder fuzzySecurity_answer (String ... fuzzySecurity_answer){
            this.fuzzySecurity_answer = solveNullList(fuzzySecurity_answer);
            return this;
        }

        public ConditionBuilder rightFuzzySecurity_answer (List<String> rightFuzzySecurity_answer){
            this.rightFuzzySecurity_answer = rightFuzzySecurity_answer;
            return this;
        }

        public ConditionBuilder rightFuzzySecurity_answer (String ... rightFuzzySecurity_answer){
            this.rightFuzzySecurity_answer = solveNullList(rightFuzzySecurity_answer);
            return this;
        }

        public ConditionBuilder security_answerList(String ... security_answer){
            this.security_answerList = solveNullList(security_answer);
            return this;
        }

        public ConditionBuilder security_answerList(List<String> security_answer){
            this.security_answerList = security_answer;
            return this;
        }

        public ConditionBuilder fuzzyPassword (List<String> fuzzyPassword){
            this.fuzzyPassword = fuzzyPassword;
            return this;
        }

        public ConditionBuilder fuzzyPassword (String ... fuzzyPassword){
            this.fuzzyPassword = solveNullList(fuzzyPassword);
            return this;
        }

        public ConditionBuilder rightFuzzyPassword (List<String> rightFuzzyPassword){
            this.rightFuzzyPassword = rightFuzzyPassword;
            return this;
        }

        public ConditionBuilder rightFuzzyPassword (String ... rightFuzzyPassword){
            this.rightFuzzyPassword = solveNullList(rightFuzzyPassword);
            return this;
        }

        public ConditionBuilder passwordList(String ... password){
            this.passwordList = solveNullList(password);
            return this;
        }

        public ConditionBuilder passwordList(List<String> password){
            this.passwordList = password;
            return this;
        }

        public ConditionBuilder deletedBetWeen(Integer deletedSt,Integer deletedEd){
            this.deletedSt = deletedSt;
            this.deletedEd = deletedEd;
            return this;
        }

        public ConditionBuilder deletedGreaterEqThan(Integer deletedSt){
            this.deletedSt = deletedSt;
            return this;
        }
        public ConditionBuilder deletedLessEqThan(Integer deletedEd){
            this.deletedEd = deletedEd;
            return this;
        }


        public ConditionBuilder deletedList(Integer ... deleted){
            this.deletedList = solveNullList(deleted);
            return this;
        }

        public ConditionBuilder deletedList(List<Integer> deleted){
            this.deletedList = deleted;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private TotalRole obj;

        public Builder(){
            this.obj = new TotalRole();
        }

        public Builder account(String account){
            this.obj.setAccount(account);
            return this;
        }
        public Builder id(String id){
            this.obj.setId(id);
            return this;
        }
        public Builder status(Integer status){
            this.obj.setStatus(status);
            return this;
        }
        public Builder security_question(String security_question){
            this.obj.setSecurity_question(security_question);
            return this;
        }
        public Builder role(Integer role){
            this.obj.setRole(role);
            return this;
        }
        public Builder phone_number(String phone_number){
            this.obj.setPhone_number(phone_number);
            return this;
        }
        public Builder security_answer(String security_answer){
            this.obj.setSecurity_answer(security_answer);
            return this;
        }
        public Builder password(String password){
            this.obj.setPassword(password);
            return this;
        }
        public Builder deleted(Integer deleted){
            this.obj.setDeleted(deleted);
            return this;
        }
        public TotalRole build(){return obj;}
    }

}
