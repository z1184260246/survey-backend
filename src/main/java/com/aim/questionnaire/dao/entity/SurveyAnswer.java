package com.aim.questionnaire.dao.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class SurveyAnswer implements Serializable {

    private static final long serialVersionUID = 1657125716883L;


    /**
    * 主键
    * 问卷回答表主键
    * isNullAble:0
    */
    private String id;

    /**
    * 问卷id
    * isNullAble:1
    */
    private String surveyId;

    /**
    * ip 地址
    * isNullAble:0
    */
    private String ip;

    /**
    * 开始时间
    * isNullAble:1
    */
    private java.time.LocalDateTime startTime;

    /**
    * 结束时间
    * isNullAble:1
    */
    private java.time.LocalDateTime endTime;

    /**
    * 问卷回答
    * isNullAble:1
    */
    private String answer;


    public void setId(String id){this.id = id;}

    public String getId(){return this.id;}

    public void setSurveyId(String surveyId){this.surveyId = surveyId;}

    public String getSurveyId(){return this.surveyId;}

    public void setIp(String ip){this.ip = ip;}

    public String getIp(){return this.ip;}

    public void setStartTime(java.time.LocalDateTime startTime){this.startTime = startTime;}

    public java.time.LocalDateTime getStartTime(){return this.startTime;}

    public void setEndTime(java.time.LocalDateTime endTime){this.endTime = endTime;}

    public java.time.LocalDateTime getEndTime(){return this.endTime;}

    public void setAnswer(String answer){this.answer = answer;}

    public String getAnswer(){return this.answer;}
    @Override
    public String toString() {
        return "SurveyAnswer{" +
                "id='" + id + '\'' +
                "surveyId='" + surveyId + '\'' +
                "ip='" + ip + '\'' +
                "startTime='" + startTime + '\'' +
                "endTime='" + endTime + '\'' +
                "answer='" + answer + '\'' +
            '}';
    }

    public static Builder bBuild(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder queryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private SurveyAnswer set;

        private ConditionBuilder where;

        public UpdateBuilder set(SurveyAnswer set){
            this.set = set;
            return this;
        }

        public SurveyAnswer getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends SurveyAnswer{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> surveyIdList;

        public List<String> getSurveyIdList(){return this.surveyIdList;}


        private List<String> fuzzySurveyId;

        public List<String> getFuzzySurveyId(){return this.fuzzySurveyId;}

        private List<String> rightFuzzySurveyId;

        public List<String> getRightFuzzySurveyId(){return this.rightFuzzySurveyId;}
        private List<String> ipList;

        public List<String> getIpList(){return this.ipList;}


        private List<String> fuzzyIp;

        public List<String> getFuzzyIp(){return this.fuzzyIp;}

        private List<String> rightFuzzyIp;

        public List<String> getRightFuzzyIp(){return this.rightFuzzyIp;}
        private List<java.time.LocalDateTime> startTimeList;

        public List<java.time.LocalDateTime> getStartTimeList(){return this.startTimeList;}

        private java.time.LocalDateTime startTimeSt;

        private java.time.LocalDateTime startTimeEd;

        public java.time.LocalDateTime getStartTimeSt(){return this.startTimeSt;}

        public java.time.LocalDateTime getStartTimeEd(){return this.startTimeEd;}

        private List<java.time.LocalDateTime> endTimeList;

        public List<java.time.LocalDateTime> getEndTimeList(){return this.endTimeList;}

        private java.time.LocalDateTime endTimeSt;

        private java.time.LocalDateTime endTimeEd;

        public java.time.LocalDateTime getEndTimeSt(){return this.endTimeSt;}

        public java.time.LocalDateTime getEndTimeEd(){return this.endTimeEd;}

        private List<String> answerList;

        public List<String> getAnswerList(){return this.answerList;}


        private List<String> fuzzyAnswer;

        public List<String> getFuzzyAnswer(){return this.fuzzyAnswer;}

        private List<String> rightFuzzyAnswer;

        public List<String> getRightFuzzyAnswer(){return this.rightFuzzyAnswer;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public QueryBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public QueryBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public QueryBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public QueryBuilder id(String id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder fuzzySurveyId (List<String> fuzzySurveyId){
            this.fuzzySurveyId = fuzzySurveyId;
            return this;
        }

        public QueryBuilder fuzzySurveyId (String ... fuzzySurveyId){
            this.fuzzySurveyId = solveNullList(fuzzySurveyId);
            return this;
        }

        public QueryBuilder rightFuzzySurveyId (List<String> rightFuzzySurveyId){
            this.rightFuzzySurveyId = rightFuzzySurveyId;
            return this;
        }

        public QueryBuilder rightFuzzySurveyId (String ... rightFuzzySurveyId){
            this.rightFuzzySurveyId = solveNullList(rightFuzzySurveyId);
            return this;
        }

        public QueryBuilder surveyId(String surveyId){
            setSurveyId(surveyId);
            return this;
        }

        public QueryBuilder surveyIdList(String ... surveyId){
            this.surveyIdList = solveNullList(surveyId);
            return this;
        }

        public QueryBuilder surveyIdList(List<String> surveyId){
            this.surveyIdList = surveyId;
            return this;
        }

        public QueryBuilder fetchSurveyId(){
            setFetchFields("fetchFields","surveyId");
            return this;
        }

        public QueryBuilder excludeSurveyId(){
            setFetchFields("excludeFields","surveyId");
            return this;
        }

        public QueryBuilder fuzzyIp (List<String> fuzzyIp){
            this.fuzzyIp = fuzzyIp;
            return this;
        }

        public QueryBuilder fuzzyIp (String ... fuzzyIp){
            this.fuzzyIp = solveNullList(fuzzyIp);
            return this;
        }

        public QueryBuilder rightFuzzyIp (List<String> rightFuzzyIp){
            this.rightFuzzyIp = rightFuzzyIp;
            return this;
        }

        public QueryBuilder rightFuzzyIp (String ... rightFuzzyIp){
            this.rightFuzzyIp = solveNullList(rightFuzzyIp);
            return this;
        }

        public QueryBuilder ip(String ip){
            setIp(ip);
            return this;
        }

        public QueryBuilder ipList(String ... ip){
            this.ipList = solveNullList(ip);
            return this;
        }

        public QueryBuilder ipList(List<String> ip){
            this.ipList = ip;
            return this;
        }

        public QueryBuilder fetchIp(){
            setFetchFields("fetchFields","ip");
            return this;
        }

        public QueryBuilder excludeIp(){
            setFetchFields("excludeFields","ip");
            return this;
        }

        public QueryBuilder startTimeBetWeen(java.time.LocalDateTime startTimeSt,java.time.LocalDateTime startTimeEd){
            this.startTimeSt = startTimeSt;
            this.startTimeEd = startTimeEd;
            return this;
        }

        public QueryBuilder startTimeGreaterEqThan(java.time.LocalDateTime startTimeSt){
            this.startTimeSt = startTimeSt;
            return this;
        }
        public QueryBuilder startTimeLessEqThan(java.time.LocalDateTime startTimeEd){
            this.startTimeEd = startTimeEd;
            return this;
        }


        public QueryBuilder startTime(java.time.LocalDateTime startTime){
            setStartTime(startTime);
            return this;
        }

        public QueryBuilder startTimeList(java.time.LocalDateTime ... startTime){
            this.startTimeList = solveNullList(startTime);
            return this;
        }

        public QueryBuilder startTimeList(List<java.time.LocalDateTime> startTime){
            this.startTimeList = startTime;
            return this;
        }

        public QueryBuilder fetchStartTime(){
            setFetchFields("fetchFields","startTime");
            return this;
        }

        public QueryBuilder excludeStartTime(){
            setFetchFields("excludeFields","startTime");
            return this;
        }

        public QueryBuilder endTimeBetWeen(java.time.LocalDateTime endTimeSt,java.time.LocalDateTime endTimeEd){
            this.endTimeSt = endTimeSt;
            this.endTimeEd = endTimeEd;
            return this;
        }

        public QueryBuilder endTimeGreaterEqThan(java.time.LocalDateTime endTimeSt){
            this.endTimeSt = endTimeSt;
            return this;
        }
        public QueryBuilder endTimeLessEqThan(java.time.LocalDateTime endTimeEd){
            this.endTimeEd = endTimeEd;
            return this;
        }


        public QueryBuilder endTime(java.time.LocalDateTime endTime){
            setEndTime(endTime);
            return this;
        }

        public QueryBuilder endTimeList(java.time.LocalDateTime ... endTime){
            this.endTimeList = solveNullList(endTime);
            return this;
        }

        public QueryBuilder endTimeList(List<java.time.LocalDateTime> endTime){
            this.endTimeList = endTime;
            return this;
        }

        public QueryBuilder fetchEndTime(){
            setFetchFields("fetchFields","endTime");
            return this;
        }

        public QueryBuilder excludeEndTime(){
            setFetchFields("excludeFields","endTime");
            return this;
        }

        public QueryBuilder fuzzyAnswer (List<String> fuzzyAnswer){
            this.fuzzyAnswer = fuzzyAnswer;
            return this;
        }

        public QueryBuilder fuzzyAnswer (String ... fuzzyAnswer){
            this.fuzzyAnswer = solveNullList(fuzzyAnswer);
            return this;
        }

        public QueryBuilder rightFuzzyAnswer (List<String> rightFuzzyAnswer){
            this.rightFuzzyAnswer = rightFuzzyAnswer;
            return this;
        }

        public QueryBuilder rightFuzzyAnswer (String ... rightFuzzyAnswer){
            this.rightFuzzyAnswer = solveNullList(rightFuzzyAnswer);
            return this;
        }

        public QueryBuilder answer(String answer){
            setAnswer(answer);
            return this;
        }

        public QueryBuilder answerList(String ... answer){
            this.answerList = solveNullList(answer);
            return this;
        }

        public QueryBuilder answerList(List<String> answer){
            this.answerList = answer;
            return this;
        }

        public QueryBuilder fetchAnswer(){
            setFetchFields("fetchFields","answer");
            return this;
        }

        public QueryBuilder excludeAnswer(){
            setFetchFields("excludeFields","answer");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public SurveyAnswer build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> surveyIdList;

        public List<String> getSurveyIdList(){return this.surveyIdList;}


        private List<String> fuzzySurveyId;

        public List<String> getFuzzySurveyId(){return this.fuzzySurveyId;}

        private List<String> rightFuzzySurveyId;

        public List<String> getRightFuzzySurveyId(){return this.rightFuzzySurveyId;}
        private List<String> ipList;

        public List<String> getIpList(){return this.ipList;}


        private List<String> fuzzyIp;

        public List<String> getFuzzyIp(){return this.fuzzyIp;}

        private List<String> rightFuzzyIp;

        public List<String> getRightFuzzyIp(){return this.rightFuzzyIp;}
        private List<java.time.LocalDateTime> startTimeList;

        public List<java.time.LocalDateTime> getStartTimeList(){return this.startTimeList;}

        private java.time.LocalDateTime startTimeSt;

        private java.time.LocalDateTime startTimeEd;

        public java.time.LocalDateTime getStartTimeSt(){return this.startTimeSt;}

        public java.time.LocalDateTime getStartTimeEd(){return this.startTimeEd;}

        private List<java.time.LocalDateTime> endTimeList;

        public List<java.time.LocalDateTime> getEndTimeList(){return this.endTimeList;}

        private java.time.LocalDateTime endTimeSt;

        private java.time.LocalDateTime endTimeEd;

        public java.time.LocalDateTime getEndTimeSt(){return this.endTimeSt;}

        public java.time.LocalDateTime getEndTimeEd(){return this.endTimeEd;}

        private List<String> answerList;

        public List<String> getAnswerList(){return this.answerList;}


        private List<String> fuzzyAnswer;

        public List<String> getFuzzyAnswer(){return this.fuzzyAnswer;}

        private List<String> rightFuzzyAnswer;

        public List<String> getRightFuzzyAnswer(){return this.rightFuzzyAnswer;}

        public ConditionBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public ConditionBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public ConditionBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public ConditionBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public ConditionBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder fuzzySurveyId (List<String> fuzzySurveyId){
            this.fuzzySurveyId = fuzzySurveyId;
            return this;
        }

        public ConditionBuilder fuzzySurveyId (String ... fuzzySurveyId){
            this.fuzzySurveyId = solveNullList(fuzzySurveyId);
            return this;
        }

        public ConditionBuilder rightFuzzySurveyId (List<String> rightFuzzySurveyId){
            this.rightFuzzySurveyId = rightFuzzySurveyId;
            return this;
        }

        public ConditionBuilder rightFuzzySurveyId (String ... rightFuzzySurveyId){
            this.rightFuzzySurveyId = solveNullList(rightFuzzySurveyId);
            return this;
        }

        public ConditionBuilder surveyIdList(String ... surveyId){
            this.surveyIdList = solveNullList(surveyId);
            return this;
        }

        public ConditionBuilder surveyIdList(List<String> surveyId){
            this.surveyIdList = surveyId;
            return this;
        }

        public ConditionBuilder fuzzyIp (List<String> fuzzyIp){
            this.fuzzyIp = fuzzyIp;
            return this;
        }

        public ConditionBuilder fuzzyIp (String ... fuzzyIp){
            this.fuzzyIp = solveNullList(fuzzyIp);
            return this;
        }

        public ConditionBuilder rightFuzzyIp (List<String> rightFuzzyIp){
            this.rightFuzzyIp = rightFuzzyIp;
            return this;
        }

        public ConditionBuilder rightFuzzyIp (String ... rightFuzzyIp){
            this.rightFuzzyIp = solveNullList(rightFuzzyIp);
            return this;
        }

        public ConditionBuilder ipList(String ... ip){
            this.ipList = solveNullList(ip);
            return this;
        }

        public ConditionBuilder ipList(List<String> ip){
            this.ipList = ip;
            return this;
        }

        public ConditionBuilder startTimeBetWeen(java.time.LocalDateTime startTimeSt,java.time.LocalDateTime startTimeEd){
            this.startTimeSt = startTimeSt;
            this.startTimeEd = startTimeEd;
            return this;
        }

        public ConditionBuilder startTimeGreaterEqThan(java.time.LocalDateTime startTimeSt){
            this.startTimeSt = startTimeSt;
            return this;
        }
        public ConditionBuilder startTimeLessEqThan(java.time.LocalDateTime startTimeEd){
            this.startTimeEd = startTimeEd;
            return this;
        }


        public ConditionBuilder startTimeList(java.time.LocalDateTime ... startTime){
            this.startTimeList = solveNullList(startTime);
            return this;
        }

        public ConditionBuilder startTimeList(List<java.time.LocalDateTime> startTime){
            this.startTimeList = startTime;
            return this;
        }

        public ConditionBuilder endTimeBetWeen(java.time.LocalDateTime endTimeSt,java.time.LocalDateTime endTimeEd){
            this.endTimeSt = endTimeSt;
            this.endTimeEd = endTimeEd;
            return this;
        }

        public ConditionBuilder endTimeGreaterEqThan(java.time.LocalDateTime endTimeSt){
            this.endTimeSt = endTimeSt;
            return this;
        }
        public ConditionBuilder endTimeLessEqThan(java.time.LocalDateTime endTimeEd){
            this.endTimeEd = endTimeEd;
            return this;
        }


        public ConditionBuilder endTimeList(java.time.LocalDateTime ... endTime){
            this.endTimeList = solveNullList(endTime);
            return this;
        }

        public ConditionBuilder endTimeList(List<java.time.LocalDateTime> endTime){
            this.endTimeList = endTime;
            return this;
        }

        public ConditionBuilder fuzzyAnswer (List<String> fuzzyAnswer){
            this.fuzzyAnswer = fuzzyAnswer;
            return this;
        }

        public ConditionBuilder fuzzyAnswer (String ... fuzzyAnswer){
            this.fuzzyAnswer = solveNullList(fuzzyAnswer);
            return this;
        }

        public ConditionBuilder rightFuzzyAnswer (List<String> rightFuzzyAnswer){
            this.rightFuzzyAnswer = rightFuzzyAnswer;
            return this;
        }

        public ConditionBuilder rightFuzzyAnswer (String ... rightFuzzyAnswer){
            this.rightFuzzyAnswer = solveNullList(rightFuzzyAnswer);
            return this;
        }

        public ConditionBuilder answerList(String ... answer){
            this.answerList = solveNullList(answer);
            return this;
        }

        public ConditionBuilder answerList(List<String> answer){
            this.answerList = answer;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private SurveyAnswer obj;

        public Builder(){
            this.obj = new SurveyAnswer();
        }

        public Builder id(String id){
            this.obj.setId(id);
            return this;
        }
        public Builder surveyId(String surveyId){
            this.obj.setSurveyId(surveyId);
            return this;
        }
        public Builder ip(String ip){
            this.obj.setIp(ip);
            return this;
        }
        public Builder startTime(java.time.LocalDateTime startTime){
            this.obj.setStartTime(startTime);
            return this;
        }
        public Builder endTime(java.time.LocalDateTime endTime){
            this.obj.setEndTime(endTime);
            return this;
        }
        public Builder answer(String answer){
            this.obj.setAnswer(answer);
            return this;
        }
        public SurveyAnswer build(){return obj;}
    }

}
