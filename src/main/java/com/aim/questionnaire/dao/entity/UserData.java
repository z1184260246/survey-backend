package com.aim.questionnaire.dao.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class UserData implements Serializable {

    private static final long serialVersionUID = 1656541822377L;


    /**
    * 主键
    * 用户id主键
    * isNullAble:0
    */
    private String id;

    /**
    * 账户
    * isNullAble:1
    */
    private String account;

    /**
    * 密码
    * isNullAble:1
    */
    private String password;

    /**
    * 用户身份
    * isNullAble:1
    */
    private String role;

    /**
    * 用户token
    * isNullAble:0
    */
    private String token;


    public void setId(String id){this.id = id;}

    public String getId(){return this.id;}

    public void setAccount(String account){this.account = account;}

    public String getAccount(){return this.account;}

    public void setPassword(String password){this.password = password;}

    public String getPassword(){return this.password;}

    public void setRole(String role){this.role = role;}

    public String getRole(){return this.role;}

    public void setToken(String token){this.token = token;}

    public String getToken(){return this.token;}
    @Override
    public String toString() {
        return "UserData{" +
                "id='" + id + '\'' +
                "account='" + account + '\'' +
                "password='" + password + '\'' +
                "role='" + role + '\'' +
                "token='" + token + '\'' +
            '}';
    }

    public static Builder bBuild(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder queryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private UserData set;

        private ConditionBuilder where;

        public UpdateBuilder set(UserData set){
            this.set = set;
            return this;
        }

        public UserData getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends UserData{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> accountList;

        public List<String> getAccountList(){return this.accountList;}


        private List<String> fuzzyAccount;

        public List<String> getFuzzyAccount(){return this.fuzzyAccount;}

        private List<String> rightFuzzyAccount;

        public List<String> getRightFuzzyAccount(){return this.rightFuzzyAccount;}
        private List<String> passwordList;

        public List<String> getPasswordList(){return this.passwordList;}


        private List<String> fuzzyPassword;

        public List<String> getFuzzyPassword(){return this.fuzzyPassword;}

        private List<String> rightFuzzyPassword;

        public List<String> getRightFuzzyPassword(){return this.rightFuzzyPassword;}
        private List<String> roleList;

        public List<String> getRoleList(){return this.roleList;}


        private List<String> fuzzyRole;

        public List<String> getFuzzyRole(){return this.fuzzyRole;}

        private List<String> rightFuzzyRole;

        public List<String> getRightFuzzyRole(){return this.rightFuzzyRole;}
        private List<String> tokenList;

        public List<String> getTokenList(){return this.tokenList;}


        private List<String> fuzzyToken;

        public List<String> getFuzzyToken(){return this.fuzzyToken;}

        private List<String> rightFuzzyToken;

        public List<String> getRightFuzzyToken(){return this.rightFuzzyToken;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public QueryBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public QueryBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public QueryBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public QueryBuilder id(String id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder fuzzyAccount (List<String> fuzzyAccount){
            this.fuzzyAccount = fuzzyAccount;
            return this;
        }

        public QueryBuilder fuzzyAccount (String ... fuzzyAccount){
            this.fuzzyAccount = solveNullList(fuzzyAccount);
            return this;
        }

        public QueryBuilder rightFuzzyAccount (List<String> rightFuzzyAccount){
            this.rightFuzzyAccount = rightFuzzyAccount;
            return this;
        }

        public QueryBuilder rightFuzzyAccount (String ... rightFuzzyAccount){
            this.rightFuzzyAccount = solveNullList(rightFuzzyAccount);
            return this;
        }

        public QueryBuilder account(String account){
            setAccount(account);
            return this;
        }

        public QueryBuilder accountList(String ... account){
            this.accountList = solveNullList(account);
            return this;
        }

        public QueryBuilder accountList(List<String> account){
            this.accountList = account;
            return this;
        }

        public QueryBuilder fetchAccount(){
            setFetchFields("fetchFields","account");
            return this;
        }

        public QueryBuilder excludeAccount(){
            setFetchFields("excludeFields","account");
            return this;
        }

        public QueryBuilder fuzzyPassword (List<String> fuzzyPassword){
            this.fuzzyPassword = fuzzyPassword;
            return this;
        }

        public QueryBuilder fuzzyPassword (String ... fuzzyPassword){
            this.fuzzyPassword = solveNullList(fuzzyPassword);
            return this;
        }

        public QueryBuilder rightFuzzyPassword (List<String> rightFuzzyPassword){
            this.rightFuzzyPassword = rightFuzzyPassword;
            return this;
        }

        public QueryBuilder rightFuzzyPassword (String ... rightFuzzyPassword){
            this.rightFuzzyPassword = solveNullList(rightFuzzyPassword);
            return this;
        }

        public QueryBuilder password(String password){
            setPassword(password);
            return this;
        }

        public QueryBuilder passwordList(String ... password){
            this.passwordList = solveNullList(password);
            return this;
        }

        public QueryBuilder passwordList(List<String> password){
            this.passwordList = password;
            return this;
        }

        public QueryBuilder fetchPassword(){
            setFetchFields("fetchFields","password");
            return this;
        }

        public QueryBuilder excludePassword(){
            setFetchFields("excludeFields","password");
            return this;
        }

        public QueryBuilder fuzzyRole (List<String> fuzzyRole){
            this.fuzzyRole = fuzzyRole;
            return this;
        }

        public QueryBuilder fuzzyRole (String ... fuzzyRole){
            this.fuzzyRole = solveNullList(fuzzyRole);
            return this;
        }

        public QueryBuilder rightFuzzyRole (List<String> rightFuzzyRole){
            this.rightFuzzyRole = rightFuzzyRole;
            return this;
        }

        public QueryBuilder rightFuzzyRole (String ... rightFuzzyRole){
            this.rightFuzzyRole = solveNullList(rightFuzzyRole);
            return this;
        }

        public QueryBuilder role(String role){
            setRole(role);
            return this;
        }

        public QueryBuilder roleList(String ... role){
            this.roleList = solveNullList(role);
            return this;
        }

        public QueryBuilder roleList(List<String> role){
            this.roleList = role;
            return this;
        }

        public QueryBuilder fetchRole(){
            setFetchFields("fetchFields","role");
            return this;
        }

        public QueryBuilder excludeRole(){
            setFetchFields("excludeFields","role");
            return this;
        }

        public QueryBuilder fuzzyToken (List<String> fuzzyToken){
            this.fuzzyToken = fuzzyToken;
            return this;
        }

        public QueryBuilder fuzzyToken (String ... fuzzyToken){
            this.fuzzyToken = solveNullList(fuzzyToken);
            return this;
        }

        public QueryBuilder rightFuzzyToken (List<String> rightFuzzyToken){
            this.rightFuzzyToken = rightFuzzyToken;
            return this;
        }

        public QueryBuilder rightFuzzyToken (String ... rightFuzzyToken){
            this.rightFuzzyToken = solveNullList(rightFuzzyToken);
            return this;
        }

        public QueryBuilder token(String token){
            setToken(token);
            return this;
        }

        public QueryBuilder tokenList(String ... token){
            this.tokenList = solveNullList(token);
            return this;
        }

        public QueryBuilder tokenList(List<String> token){
            this.tokenList = token;
            return this;
        }

        public QueryBuilder fetchToken(){
            setFetchFields("fetchFields","token");
            return this;
        }

        public QueryBuilder excludeToken(){
            setFetchFields("excludeFields","token");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public UserData build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> accountList;

        public List<String> getAccountList(){return this.accountList;}


        private List<String> fuzzyAccount;

        public List<String> getFuzzyAccount(){return this.fuzzyAccount;}

        private List<String> rightFuzzyAccount;

        public List<String> getRightFuzzyAccount(){return this.rightFuzzyAccount;}
        private List<String> passwordList;

        public List<String> getPasswordList(){return this.passwordList;}


        private List<String> fuzzyPassword;

        public List<String> getFuzzyPassword(){return this.fuzzyPassword;}

        private List<String> rightFuzzyPassword;

        public List<String> getRightFuzzyPassword(){return this.rightFuzzyPassword;}
        private List<String> roleList;

        public List<String> getRoleList(){return this.roleList;}


        private List<String> fuzzyRole;

        public List<String> getFuzzyRole(){return this.fuzzyRole;}

        private List<String> rightFuzzyRole;

        public List<String> getRightFuzzyRole(){return this.rightFuzzyRole;}
        private List<String> tokenList;

        public List<String> getTokenList(){return this.tokenList;}


        private List<String> fuzzyToken;

        public List<String> getFuzzyToken(){return this.fuzzyToken;}

        private List<String> rightFuzzyToken;

        public List<String> getRightFuzzyToken(){return this.rightFuzzyToken;}

        public ConditionBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public ConditionBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public ConditionBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public ConditionBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public ConditionBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder fuzzyAccount (List<String> fuzzyAccount){
            this.fuzzyAccount = fuzzyAccount;
            return this;
        }

        public ConditionBuilder fuzzyAccount (String ... fuzzyAccount){
            this.fuzzyAccount = solveNullList(fuzzyAccount);
            return this;
        }

        public ConditionBuilder rightFuzzyAccount (List<String> rightFuzzyAccount){
            this.rightFuzzyAccount = rightFuzzyAccount;
            return this;
        }

        public ConditionBuilder rightFuzzyAccount (String ... rightFuzzyAccount){
            this.rightFuzzyAccount = solveNullList(rightFuzzyAccount);
            return this;
        }

        public ConditionBuilder accountList(String ... account){
            this.accountList = solveNullList(account);
            return this;
        }

        public ConditionBuilder accountList(List<String> account){
            this.accountList = account;
            return this;
        }

        public ConditionBuilder fuzzyPassword (List<String> fuzzyPassword){
            this.fuzzyPassword = fuzzyPassword;
            return this;
        }

        public ConditionBuilder fuzzyPassword (String ... fuzzyPassword){
            this.fuzzyPassword = solveNullList(fuzzyPassword);
            return this;
        }

        public ConditionBuilder rightFuzzyPassword (List<String> rightFuzzyPassword){
            this.rightFuzzyPassword = rightFuzzyPassword;
            return this;
        }

        public ConditionBuilder rightFuzzyPassword (String ... rightFuzzyPassword){
            this.rightFuzzyPassword = solveNullList(rightFuzzyPassword);
            return this;
        }

        public ConditionBuilder passwordList(String ... password){
            this.passwordList = solveNullList(password);
            return this;
        }

        public ConditionBuilder passwordList(List<String> password){
            this.passwordList = password;
            return this;
        }

        public ConditionBuilder fuzzyRole (List<String> fuzzyRole){
            this.fuzzyRole = fuzzyRole;
            return this;
        }

        public ConditionBuilder fuzzyRole (String ... fuzzyRole){
            this.fuzzyRole = solveNullList(fuzzyRole);
            return this;
        }

        public ConditionBuilder rightFuzzyRole (List<String> rightFuzzyRole){
            this.rightFuzzyRole = rightFuzzyRole;
            return this;
        }

        public ConditionBuilder rightFuzzyRole (String ... rightFuzzyRole){
            this.rightFuzzyRole = solveNullList(rightFuzzyRole);
            return this;
        }

        public ConditionBuilder roleList(String ... role){
            this.roleList = solveNullList(role);
            return this;
        }

        public ConditionBuilder roleList(List<String> role){
            this.roleList = role;
            return this;
        }

        public ConditionBuilder fuzzyToken (List<String> fuzzyToken){
            this.fuzzyToken = fuzzyToken;
            return this;
        }

        public ConditionBuilder fuzzyToken (String ... fuzzyToken){
            this.fuzzyToken = solveNullList(fuzzyToken);
            return this;
        }

        public ConditionBuilder rightFuzzyToken (List<String> rightFuzzyToken){
            this.rightFuzzyToken = rightFuzzyToken;
            return this;
        }

        public ConditionBuilder rightFuzzyToken (String ... rightFuzzyToken){
            this.rightFuzzyToken = solveNullList(rightFuzzyToken);
            return this;
        }

        public ConditionBuilder tokenList(String ... token){
            this.tokenList = solveNullList(token);
            return this;
        }

        public ConditionBuilder tokenList(List<String> token){
            this.tokenList = token;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private UserData obj;

        public Builder(){
            this.obj = new UserData();
        }

        public Builder id(String id){
            this.obj.setId(id);
            return this;
        }
        public Builder account(String account){
            this.obj.setAccount(account);
            return this;
        }
        public Builder password(String password){
            this.obj.setPassword(password);
            return this;
        }
        public Builder role(String role){
            this.obj.setRole(role);
            return this;
        }
        public Builder token(String token){
            this.obj.setToken(token);
            return this;
        }
        public UserData build(){return obj;}
    }

}
