package com.aim.questionnaire.dao.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.Tenant;
/**
*  @author author
*/
public interface TenantBaseMapper {

    int insertTenant(Tenant object);

    int updateTenant(Tenant object);

    int update(Tenant.UpdateBuilder object);

    List<Tenant> queryTenant(Tenant object);

    Tenant queryTenantLimit1(Tenant object);

}