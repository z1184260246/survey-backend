package com.aim.questionnaire.dao.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.Group;
/**
*  @author author
*/
public interface GroupBaseMapper {

    int insertGroup(Group object);

    int updateGroup(Group object);

    int update(Group.UpdateBuilder object);

    List<Group> queryGroup(Group object);

    Group queryGroupLimit1(Group object);

}