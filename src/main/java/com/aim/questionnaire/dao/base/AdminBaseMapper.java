package com.aim.questionnaire.dao.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.Admin;
/**
*  @author author
*/
public interface AdminBaseMapper {

    int insertAdmin(Admin object);

    int updateAdmin(Admin object);

    int update(Admin.UpdateBuilder object);

    List<Admin> queryAdmin(Admin object);

    Admin queryAdminLimit1(Admin object);

}