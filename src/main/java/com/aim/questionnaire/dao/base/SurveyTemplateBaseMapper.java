package com.aim.questionnaire.dao.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.SurveyTemplate;
/**
*  @author author
*/
public interface SurveyTemplateBaseMapper {

    int insertSurveyTemplate(SurveyTemplate object);

    int updateSurveyTemplate(SurveyTemplate object);

    int update(SurveyTemplate.UpdateBuilder object);

    List<SurveyTemplate> querySurveyTemplate(SurveyTemplate object);

    SurveyTemplate querySurveyTemplateLimit1(SurveyTemplate object);

}
