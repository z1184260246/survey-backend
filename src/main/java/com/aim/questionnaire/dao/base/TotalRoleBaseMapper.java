package com.aim.questionnaire.dao.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.TotalRole;
/**
*  @author author
*/
public interface TotalRoleBaseMapper {

    int insertTotalRole(TotalRole object);

    int updateTotalRole(TotalRole object);

    int update(TotalRole.UpdateBuilder object);

    List<TotalRole> queryTotalRole(TotalRole object);

    TotalRole queryTotalRoleLimit1(TotalRole object);

}